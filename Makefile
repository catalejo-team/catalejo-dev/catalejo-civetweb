civetweb-install:
	bash ./civetweb.bash download
	bash ./civetweb.bash build-linux
	bash ./civetweb.bash install-linux

lua-install:
	bash ./lua.bash dependencies
	bash ./lua.bash download
	bash ./lua.bash build-linux
	bash ./lua.bash install-linux

tree-install:
	bash ./tree.bash download
	bash ./tree.bash build-linux
	bash ./tree.bash install-linux
