#!/bin/bash

APP=tree
SCRIPT_PATH=$(pwd)
BUILDER_PATH=$SCRIPT_PATH/build-apps
DIR_APP=$BUILDER_PATH/$APP
BIN=$SCRIPT_PATH/bin
LIB=$SCRIPT_PATH/lib
DOWNLOAD_APP="https://gitlab.com/OldManProgrammer/unix-tree/-/archive/master/unix-tree-master.tar.gz"

dependencies() {
	sudo apt update
	sudo apt install \
		libreadline-dev \
		-y
}

download() {
	mkdir -p $BUILDER_PATH
	cd $BUILDER_PATH
	rm -rf $DIR_APP
	wget -O $APP.tar.gz $DOWNLOAD_APP
	tar xzf $APP.tar.gz
	mv unix-tree-master $APP
}

build-linux() {
	cd $DIR_APP
	make tree
}

install-linux() {
	mkdir -p $BIN
	cd $DIR_APP
	mv $APP $BIN
}

if [[ -v 1 ]]; then
	$1
fi
