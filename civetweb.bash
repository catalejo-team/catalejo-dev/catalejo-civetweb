#!/bin/bash

APP=civetweb
SCRIPT_PATH=$(pwd)
BUILDER_PATH=$SCRIPT_PATH/build-apps
DIR_APP=$BUILDER_PATH/civetweb-master
BIN=$SCRIPT_PATH/bin
LIB=$SCRIPT_PATH/lib
DOWNLOAD_APP="https://github.com/civetweb/civetweb/archive/refs/heads/master.zip"

download() {
	mkdir -p $BUILDER_PATH
	cd $BUILDER_PATH
	rm -rf $DIR_APP
	wget -O $APP.zip $DOWNLOAD_APP
	unzip $APP.zip
}

build-linux() {
	cd $DIR_APP
	make WITH_ALL=1 build
}

install-linux() {
	mkdir -p $BIN
	cd $DIR_APP
	mv $APP $BIN
}

if [[ -v 1 ]]; then
	$1
fi
